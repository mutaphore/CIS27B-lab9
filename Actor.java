
public class Actor extends Client{
	
	private char gender;
	private int age;
	public static final char DEFAULT_GENDER = 'm';
	public static final int DEFAULT_AGE = 25;
	public static final int MAX_AGE = 70;
	public static final int MIN_AGE = 10;
	
	//Zero parameter constructor
	public Actor() {
		
		super();
		gender = DEFAULT_GENDER;
		age = DEFAULT_AGE;
	
	}
	
	//This contructor initialize just this derived class
	public Actor(char gender, int age) {
		
		super();
		if(!SetGender(gender))
			gender = DEFAULT_GENDER;
		if(!SetAge(age))
			age = DEFAULT_AGE;
		
	}
	
	//This contructor initializes base class too
	public Actor(String name, long income_this_year, double percent_cut, char gender, int age) {
		
		super(name, income_this_year, percent_cut);
		if(!SetGender(gender))
			gender = DEFAULT_GENDER;
		if(!SetAge(age))
			age = DEFAULT_AGE;
		
	}
	
	//Accessors---------------------------------------
	
	public char GetGender() {
		
		return gender;
		
	}
	
	public int GetAge() {
		
		return age;
		
	}
	
	public String toString() {
		
		StringBuffer temp = new StringBuffer();
		
		temp.append("\nActor Name: " + super.name +
						"\nThis year's income: $" + super.income_this_year +
						"\nPercent cut: " + super.percent_cut +
						"\nGender: " + gender +
						"\nAge: " + age);
		
		return temp.toString();
		
	}
	
	//Mutators---------------------------------------
	public boolean SetGender(char gender) {
		
		if(gender == 'm' || gender == 'M' || gender == 'f' || gender == 'F') {
			this.gender = gender;
			return true;
		}
		else
			return false;
		
	}
	
	public boolean SetAge(int age) {
		
		if(MIN_AGE <= age && age <= MAX_AGE) {
			this.age = age;
			return true;
		}
		else
			return false;
		
	}

}
