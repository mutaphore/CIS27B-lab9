//The property class
public class Property {
	
	//Declare class members
	public static final int MAX_DAYS_ON_MARKET = 1825;
	public static final int MAX_ROOMS = 50;
	public static final int RENTAL_CONVERSION_FACTOR = 300;
	public static final String RENTAL = "RNT";
	public static final String CONDO = "CND";
	public static final String SINGLE_FAMILY = "SFD";
	public static final String DEFAULT_TYPE = CONDO;
	public static final int DEFAULT_PRICE = 2000;
	public static final int DEFAULT_BEDROOMS = 3;
	public static final int DEFAULT_DAYS = 30;
	public static final String DEFAULT_ADDRESS = "123 Street, San Diego";
	public static final String PRICE = "byPrice";
	public static final String ROOMS = "byNumRooms";
	public static final String DAYS = "byDaysOnMarket";
	private String type;
	private int price;
	private int bedrooms;
	private int days_on_market;
	private String address;
	
	//Default Constructor
	public Property() {
		
		this.type = DEFAULT_TYPE;
		this.price = DEFAULT_PRICE;
		this.bedrooms = DEFAULT_BEDROOMS;
		this.days_on_market = DEFAULT_DAYS;
		this.address = DEFAULT_ADDRESS;
		
	}
	
	//Constructor with 5 params passed, use mutators to check validity of input: sets to default
	//values if input invalid
	public Property(String type, int price, int bedrooms, int days_on_market, String address) {
		
		if(!SetType(type))   this.type = DEFAULT_TYPE;
		if(!SetPrice(price))   this.price = DEFAULT_PRICE;
		if(!SetBedrooms(bedrooms))   this.bedrooms = DEFAULT_BEDROOMS;
		if(!SetDaysOnMarket(days_on_market))   this.days_on_market = DEFAULT_DAYS;		
		if(!SetAddress(address))   this.address = DEFAULT_ADDRESS;
		
	}
	
	//Mutators to set private members
	boolean SetType(String type) {
		
		if((!type.equals(RENTAL)) && (!type.equals(CONDO)) && (!type.equals(SINGLE_FAMILY))) 
			return false;
	
		this.type = type;
		return true;
		
	}
	
	boolean SetPrice(int price) {
		
		if(price < 0)
			return false;
		
		this.price = price;
		
		return true;
		
	}
	
	boolean SetBedrooms(int bedrooms) {
		
		if(bedrooms < 0 || bedrooms > MAX_ROOMS	)
			return false;
		this.bedrooms = bedrooms;
		return true;
		
	}

	boolean SetDaysOnMarket(int days_on_market) {
		
		if(days_on_market < 0 || days_on_market > MAX_DAYS_ON_MARKET)
			return false;
		this.days_on_market = days_on_market;
		return true;
		
	}
	
	boolean SetAddress(String address) {
		
		if(address.length() == 0 || address.length() > 100)
			return false;
		this.address = address;
		return true;
		
	}
	
	//Accessors to get private members
	
	String GetType() { return type; }
	int GetPrice() {return price; }
	int GetBedrooms() {return bedrooms; }
	int GetDaysOnMarket() {return days_on_market; }
	String GetAddress() {return address; }
	
	//This method converts current instance to rental type and price
	void ConvertToRental() {
		
		if(this.type != RENTAL) {
			this.type = RENTAL;
			this.price /= RENTAL_CONVERSION_FACTOR;
		}
		
	}
	
	//Prints the current instance property information in a sentence.
	void PrintListing() {
		
		System.out.println("--------------------------------");
		
		System.out.print("Property type: ");
		if(type.equals(RENTAL))
			System.out.print("Rental\n");
		else if(type.equals(CONDO))
			System.out.print("Condo\n");
		else
			System.out.print("Single Family Dwelling\n");
		
		System.out.println("Price: $" + price);
		System.out.println("Number of bedrooms: " + bedrooms);
		System.out.println("Days on market: " + days_on_market);
		System.out.println("Address: " + address);
		
	}
	
	//Returns a string that has all the property member values
	public String toString(){
		
		String strType;
		if(type.equals(RENTAL)) strType = "Rental\n";
		else if(type.equals(CONDO)) strType = "Condo\n";
		else if (type.equals(SINGLE_FAMILY)) strType = "Single Family Dwelling\n";
		else strType = "ERROR - Undefined property type!\n";
		
		String strProperty = "\nProperty type: " +  strType + 
		"Price: $" + price +
		"\nNumber of bedrooms: " + bedrooms + "\nDays on market: " +
		days_on_market + "\nAddress: " + address;
		
		return strProperty;
		
	}
	
	//This instance method compares this property with a passed property
	//and returns whether the key_field of the property is greater than, less than 
	//or equivalent to it
	int compareTo(Property prop, String key_field) {
		
		int returnVal;
		
		if (key_field == PRICE) {	
			if (this.price < prop.price) returnVal = -1;
			else if (this.price == prop.price) returnVal = 0;
			else returnVal = 1;
		}
		else if (key_field == DAYS) {
			if (this.days_on_market < prop.days_on_market) returnVal = -1;
			else if (this.days_on_market == prop.days_on_market) returnVal = 0;
			else returnVal = 1;
		}
		else {
			if (this.bedrooms < prop.bedrooms) returnVal = -1;
			else if (this.bedrooms == prop.bedrooms) returnVal = 0;
			else returnVal = 1;
		}
		
		return returnVal;
		
	}
	
	//Prints an array of property objects with the specified title
	public static void PrintArray(String title, Property property_list[]) {
		
		System.out.println("--------------" + title + "--------------\n");
		for (int k =0; k < property_list.length; k++)
         System.out.println( property_list[k].toString() );
      System.out.println();
      
	}
	
	//Sorts the array of property objects using selection sort with the key_field
	public static void ArraySort(Property[] property_list,  String key_field) {
		
		Property temp;
		
		for (int i=0 ; i<property_list.length ; i++) {
			
			for (int j=i ; j<property_list.length ; j++) {
				
				if(property_list[i].compareTo(property_list[j],key_field) == 1) {
					temp = property_list[j];
					property_list[j] = property_list[i];
					property_list[i] = temp;
				}
				
			}
			
		}
		
	}
	
}
