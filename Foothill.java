import java.util.*;

public class Foothill {

	public static void main(String[] args) {
		
		//Create a mixed list of Actor and Property objects
		LinkedList<Object> mixed_list = new LinkedList<Object>();
		ListIterator<Object> iterator;
		Actor a1 = new Actor("Andrew", 65000, 0.14, 'M', 29);
		Actor a2 = new Actor("Jessica", 40000, 0.2, 'F', 25);
		Property p1 = new Property(Property.CONDO, 300000, 4, 24, "123 Sunset Rd.");
		Property p2 = new Property(Property.SINGLE_FAMILY, 500000, 5, 49, "222 Main St.");
		
		mixed_list.add(a1);
		mixed_list.add(a2);
		mixed_list.add(p1);
		mixed_list.add(p2);
		
		//Print original list
		System.out.println("-------Original List-------");
		for(iterator = mixed_list.listIterator(); iterator.hasNext();)
			System.out.println(iterator.next());
		
		//Make some changes to the list
		mixed_list.remove(a1);
		mixed_list.remove(p2);
		mixed_list.add(p2);
		mixed_list.remove(p1);
		mixed_list.add(a1);
		mixed_list.add(p1);
		
		//Print updated list
		System.out.println("\n-------Updated List-------");
		for(iterator = mixed_list.listIterator(); iterator.hasNext();)
			System.out.println(iterator.next());
		
	}

}
